''' Script Google cluod platforms-GCP  '''

# Librerías
from datetime import datetime
from funciones_storage import * # funciones de GCP
import os
import sys
import shutil

def crear_bucket_blob_gcp(bucket_pais, bucket_files_iniciales):

    '''
    Crea bucket para el país que se entrena y sus subcarpetas:
          training_files: almacena archivos para entrenamiento.
          output: almacena los pesos del modelo entrenado.

    :parameters
    :param bucket_pais: nombre del país que se va a entrenar.
    :param bucket_files_iniciales: nombre bucket con archivos iniciales: .ckpts, pipeline.confin sin
           modificar, paquetes de la API comprimidos y catalogos de cada país.

    :return: rutas de gcp..
             fecha: fecha y hora de ejecución, formato: año_mes_dia_hora_minutos
             paquetes: ruta de paquetes comprimidos de la API (slim, object detection, pycocotools).
             path_training_files: ruta de archivos .ckpts, pipeline y tfrecords.
             path_file_config: ruta de pipeline sin modificar.
             path_output: ruta donde se almacenan los pesos del modelo entrenado.
             path_descarga_mvirt_catalogo: ruta local del catalogo.csv.
             dict_catalogos: directorio de los catalogos de cada país.
             path_proceso: ruta de carpeta local donde se realiza el procesamiento del programa.

    '''

    fecha = str(datetime.now().year) + '_' + str(datetime.now().month) + '_' + str(datetime.now().day) + '_' + \
            str(datetime.now().hour) + '_' + str(datetime.now().minute)

    # generar carpeta principal  donde se guarda archivos, images del proceso training
    path_proceso = os.path.join(os.getcwd(), 'train_finalizado_' + bucket_pais + '_' + fecha)

    try:

        os.mkdir(path_proceso)

    except FileExistsError:

        pass

    # borrar carpetas dentro de train_finalizado_pais_fecha
    lista_borrar = ['images_consolidadas', 'train', 'test', 'tfrecord_label_map', 'catalogo']

    for i in lista_borrar:

        try:

            shutil.rmtree(os.path.join(path_proceso, i))

        except FileNotFoundError:

            pass

    # comprobar si existe o no el bucket para crear
    existe = exist_bucket(bucket_pais)

    if existe:

        print(' *** Existe bucket con nombre:', bucket_pais, ' ***')

    else:

        # crear bucket principal
        print('bucket creado con nombre: ', bucket_pais)

        crear_bucket(bucket_pais)

    # folder pais_fecha - folder segundario
    path_pais_fecha = fecha

    crear_carpeta_bucket(bucket_pais, path_pais_fecha)

    ## subfolders en training blob
    ### training files
    path_training_files = path_pais_fecha + '/training_files'

    crear_carpeta_bucket(bucket_pais, path_training_files)

    ### folder resultados
    path_output = path_pais_fecha + '/output'

    crear_carpeta_bucket(bucket_pais, path_output)

    # ruta paquetes necesarios para entrenar el modelo (slim, object_detection,pycocotools)
    paquetes, _ = obtain_all_files_in_bucket(bucket_files_iniciales, 'paquetes')

    if len(paquetes) == 0:

        print('ERROR: no están los ficheros correspondientes en la carpeta de /paquetes')
        sys.exit()

    # De vuelve el formato que luego se necesitara en el comando del JOB
    for idx, paq in enumerate(paquetes):

        paquetes[idx] = 'gs://{}/{}'.format(bucket_files_iniciales, paq)

    # path archivo config
    path_file_config = bucket_files_iniciales + '/archivo_config/'

    # Copia  de checkpoins en el momento cero (0) (Èstos se copian a la ruta training_files )
    for name in ['index', 'meta', 'data-00000-of-00001']:

        copy_blob(bucket_files_iniciales, 'checkpoints/model.ckpt.{}'.format(name), bucket_pais, path_training_files + '/model.ckpt.{}'.format(name))

    # descargar catalogo a local
    path_descarga_mvirt_catalogo = os.path.join(path_proceso, 'catalogo')

    try:

        os.mkdir(path_descarga_mvirt_catalogo)

    except FileExistsError:

        pass

    # asignar al bucket pais: catalogo correspondiente
    dict_catalogos = {'a_colombia_prueba': 'a_colombia_prueba', 'a_col_prueba': 'BD_GT_COD_V2'}

    # descargar catalogo correspodiente a mvirtual
    download_blob(bucket_files_iniciales, os.path.join('catalogos', '{}.xlsx'.format(dict_catalogos[bucket_pais])), os.path.join(path_descarga_mvirt_catalogo, '{}.xlsx'.format(dict_catalogos[bucket_pais])))

    return (fecha, paquetes, path_training_files, path_file_config, path_output, path_descarga_mvirt_catalogo, dict_catalogos, path_proceso)


















