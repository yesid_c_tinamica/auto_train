'''
    Funciones para procesar imágenes, archivos xml, generación dataset con información
    completa de la data, generación de tfrecord/label map y enviar Job  Training - Tensorboard
'''

# Librerías
import os
import numpy as np

from funciones_storage import * # funciones de GCP client python
import sys

from p_tqdm import t_map  # gràfica barra
from handler.file import all_files_in   # convert jpg
from handler.image import is_image, convert_to_jpeg_and_override  # convert jpg
import xlrd    # para excel cotalogo
import subprocess   # para los comandos de job training
import shutil   # copiar, eliminar files
from sklearn.model_selection import train_test_split

from generate_tfrecord_labelmap import *
from modificar_pepiline import *     # actualizar paths en file pipeline.config


def mvirtual_descarga_dataset_gcp(path_images, path_proceso):
    '''

    Identificar ruta si es local o gcp y en caso ser gcp se descarga images/xml.

    :parameters
    :param path_images: ruta ingresada por el usuario de las imágenes/xml.
    :param path_proceso: ruta de carpeta donde se realiza el procesamiento del programa.

    :return: ruta local con imágenes sin procesar.

    '''


    # ruta local o gcp
    if path_images.startswith('gs://'):

        try:

            # crea carpeta donde se almancenan las imágenes crudas
            path_mvirtual_descarga_images = os.path.join(path_proceso, 'images_descargadas')
            os.mkdir(path_mvirtual_descarga_images)

        except FileExistsError:

            pass

        comando_copy_images = 'gsutil -m cp -r ' + path_images + ' ' + path_mvirtual_descarga_images
        subprocess.call(comando_copy_images, shell=True)


        path_images = path_mvirtual_descarga_images
        print('**** Finalizó descarga de imágenes y xmls - ruta: {} ****'.format(path_mvirtual_descarga_images))

    else:

        print('La ruta ingresada es local:\n', path_images)


    return path_images



def verificar_files_replicados(path_files, bucket_pais, fecha, path_proceso):
    '''

    Verifica si hay xml y/o imágenes replicados en el conjunto de datos, ademàs
    de validar si hay el mismo nùmero de imágenes y xml, en caso no, se cancela
    el programa.

    :parameters
    :param path_files: ruta carpeta con imágenes sin procesar.
    :param bucket_pais: nombre del país que se va a entrenar.
    :param fecha: fecha de ejecución (año_mes_dia_hora_min).
    :param path_proceso: ruta de carpeta donde se realiza el procesamiento

    :return: generación de dataframe con información de las replicas encontradas
             y enviada a la carpeta train_finalizado_pais_fecha y subida a gcp.

    '''

    list_acumuladora = []
    list_acumuladora_paths = []

    for path, folders, filenames in os.walk(path_files):

        if filenames != []:

            if len(filenames) > 1:

                for name in filenames:

                    if name.endswith('.xml') or is_image(os.path.join(path, name)):

                        list_acumuladora.append(name)
                        list_acumuladora_paths.append(os.path.join(path, name))

            else:

                if filenames.endswith('.xml') or is_image(os.path.join(path, name)):

                    list_acumuladora.append(filenames)
                    list_acumuladora_paths.append(os.path.join(path, filenames))


    if list_acumuladora_paths == []:

            print('*** No hay imágenes en la ruta ingresada: \n', path_files)
            sys.exit()

    # verificar que hay los archivos jpg y sus correspondientes xml
    jpgs = [i[:-4] for i in list_acumuladora if i.endswith('.jpg')]
    xmls = [i[:-4] for i in list_acumuladora if i.endswith('.xml')]

    if pd.Series(xmls)[-pd.Series(xmls).isin(jpgs)].to_list() != [] :

        print('\n *** Los siguientes archivos xml no tiene su correspondiente jpg, Verificar *** \n ',
              pd.Series(xmls)[-pd.Series(xmls).isin(jpgs)])
        sys.exit()


    # Data Frame replicas archivos
    df_tem = pd.DataFrame()
    df_tem['filename'] = list_acumuladora
    df_tem['ruta_absoluta']  = list_acumuladora_paths
    df_tem['conteo'] = 0

    groupby_filename = df_tem.groupby('filename')['conteo'].count().reset_index()
    groupby_filename = groupby_filename[groupby_filename['conteo'] > 1]
    df_tem = df_tem.drop(['conteo'], axis=1)

    df_tem = df_tem.join(groupby_filename.set_index('filename'), on='filename')
    df_tem = df_tem[df_tem['conteo'] > 1]

    if groupby_filename.empty:

        return print('*** No hay archivos replicados ***')

    else:

        # Exportar y subir archivo replicas a GCP
        path_replicas = os.path.join(path_proceso, 'archivo_replicas.csv')
        df_tem.to_csv(path_replicas, sep=',')

        subir_archivo(bucket_pais, bucket_pais + '_' + fecha, path_replicas)
        print('*** El archivo_replicas.csv tiene las rutas de los archivos replicados \n'
              ' - cargado al bucket', bucket_pais, 'de GCS ***')




def convertir_images(path):
    '''

    Convertir imágenes a .jpg

    :parameters
    :param path: ruta local carpeta con imágenes sin procesar.

    :return: imágenes convertidas a .jpg en la misma ruta 'path'.

    '''

    files = all_files_in(path)
    images = [file for file in files if is_image(file)]

    print('Convertiendo', len(images),'imágenes a jpeg')

    t_map(convert_to_jpeg_and_override, images)

    print('*** imágenes convertidas - canales y extensión ***')



def consolidar_and_mover_images_xml_convert_jpg(ruta_folders, path_proceso):
    '''

    Convertir y consolidar imágenes/xml en carpeta 'imagenes_consolidadas.

    :param ruta_folders: ruta carpeta imágenes convertidas a jpg.
    :param path_proceso: ruta de carpeta donde se realiza el procesamiento.

    :return: lista con rutas únicas de cada imagen y xml (list_acumuladora_paths)
             y ruta_destino  es la ruta de la carpeta 'imagenes_consolidadas.
    '''

    # converit images .jpg
    convertir_images(ruta_folders)

    print('*** Consolidando imágenes ***')

    list_acumuladora_paths = ['inicializar_lista']
    ruta_destino = os.path.join(path_proceso, 'imagenes_consolidadas')

    try:

        os.mkdir(ruta_destino)

    except FileExistsError:

        pass

    for path, folders, filenames in os.walk(ruta_folders):

        if filenames != []:

            if len(filenames) > 1:

                for name in filenames:

                    if name.endswith(('.xml', '.jpg')):

                        # evitar  las replicas en la lista acumuladora.
                        name_validar = [os.path.basename(i) for i in list_acumuladora_paths]

                        if not any(name_validar == np.repeat(name, len(name_validar))):

                            list_acumuladora_paths.append(os.path.join(path, name))

                            try:

                                shutil.copy(os.path.join(path, name), ruta_destino)

                            except shutil.Error:  # cuando aparece repetida la imagen

                                continue

            else:

                if filenames.endswith(('.xml', '.jpg')):

                    # evitar  las replicas en la lista acumuladora.
                    name_validar = [os.path.basename(i) for i in list_acumuladora_paths]

                    if not any(name_validar == np.repeat(filenames, len(list_acumuladora_paths))):

                        list_acumuladora_paths.append(os.path.join(path, filenames))

                        try:

                            shutil.copy(os.path.join(path, filenames), ruta_destino)

                        except shutil.Error:  # cuando aparece repetida la imagen

                            continue

    # eliminar elemento 'inicializar_lista' [0]
    list_acumuladora_paths = list_acumuladora_paths[1:]

    # elminar carpeta descargas
    #shutil.rmtree(ruta_folders)

    return (list_acumuladora_paths, ruta_destino)




def check_bbox(path_xml_mvirtual, list_acumuladora_paths):
    '''

    Lectura y extracción campos de interes en xml y generaciòn
    de dataframe por xml.

    :parameters
    :param path_xml_mvirtual: ruta ubicación xml en local o màquina virtual.
    list_acumuladora_paths: rutas originales ùnicas de xml e imàgenes.

    :return: dataframe con campos de interes de un xml.

    '''

    objeto = ET.parse(path_xml_mvirtual).findall('object')
    filename= ET.parse(path_xml_mvirtual).find('filename').text
    ancho_img = np.int(ET.parse(path_xml_mvirtual).find('size')[0].text)
    alto_img = np.int(ET.parse(path_xml_mvirtual).find('size')[1].text)
    num_replicas = len(objeto)

    id_sku = []; xmin = []; xmax = []; ymin = []; ymax = []

    for i in range(len(objeto)):
        id_sku.append(objeto[i][0].text)
        xmin.append(np.int(objeto[i].find('bndbox').find('xmin').text))
        ymin.append(np.int(objeto[i].find('bndbox').find('ymin').text))
        xmax.append(np.int(objeto[i].find('bndbox').find('xmax').text))
        ymax.append(np.int(objeto[i].find('bndbox').find('ymax').text))

    # rutas originales de los xmls/imàgenes

    path_original_xml = pd.Series(list_acumuladora_paths)[pd.Series([os.path.basename(i) for i in list_acumuladora_paths]).isin(['{}'.format(filename.split('.')[0] + '.xml')])].values[0]
    path_original_imagen = pd.Series(list_acumuladora_paths)[pd.Series([os.path.basename(i) for i in list_acumuladora_paths]).isin(['{}'.format(filename.replace('.jpeg', '.jpg'))])].values[0]

    # Data frame por xml
    df = pd.DataFrame()
    df['path_xml_origen'] = np.repeat(path_original_xml, num_replicas)
    df['path_images_origen'] = np.repeat(path_original_imagen, num_replicas)
    df['path_xml'] = np.repeat(path_xml_mvirtual, num_replicas) # usado para copiar de esta ruta a test/train
    df['filename'] = np.repeat(filename, num_replicas)
    df['filename'] = df['filename'].apply(lambda x: x.replace('.jpeg', '.jpg'))
    df['ancho_img'] = np.repeat(ancho_img, num_replicas)
    df['alto_img'] = np.repeat(alto_img, num_replicas)
    df['clase_sku'] = id_sku
    df['xmin'] = xmin
    df['ymin'] = ymin
    df['xmax'] = xmax
    df['ymax'] = ymax

    # condicional de los criterios
    tolerancia = 33
    x_value = df['xmax'] - df['xmin']
    y_value = df['ymax'] - df['ymin']

    #  alto o ancho menor a tolentancia
    df['bbox_menor_tolerancia'] = ((x_value < tolerancia) | (y_value < tolerancia))
    df['bbox_menor_tolerancia'] = df['bbox_menor_tolerancia'].apply(lambda x: 1 if x==True else 0)

    # dimensiones coordenadas bbox  y shape imagen
    df['bbox_mayor_img'] = ((df['ymin'] > df['alto_img']) | (df['ymax'] > df['alto_img']) | (df['xmin'] > df['ancho_img']) | (df['xmax'] > df['ancho_img']))
    df['bbox_mayor_img'] = df['bbox_mayor_img'].apply(lambda x: 1 if x==True else 0)

    return df




def validar_etiqueta(bucket_pais, path_descarga_mvirt_catalogo, df, dict_catalogos):
    '''

    Validar si nombre SKU está bien digitado en xml.

    :parameters
    :param bucket_pais: nombre del país que se va a entrenar.
    :param path_descarga_mvirt_catalogo: ruta local del catalogo.csv del país.
    :param df: dataframe con información extraida del xml.
    :param dict_catalogos: diccionario de catalogos.

    :return: dataframe con validaciòn de la etiqueta del SKU.

    '''

    catalogo = pd.read_excel(path_descarga_mvirt_catalogo+'/{}.xlsx'.format(dict_catalogos[bucket_pais]),index_col=0)

    # join  df con catalogo - validar etiquetas buenas
    df_val_tags = df.join(catalogo.set_index('COD DN'), on = 'clase_sku').drop(['Nombre Corregido','Sbjnum'],axis = 1)

    df_val_tags['Nombre SKU Etiquetado'] = df_val_tags['Nombre SKU Etiquetado'].replace(float('NaN'), 'error_etiqueta')
    df_val_tags = df_val_tags.rename(columns={'Nombre SKU Etiquetado':'nombre_sku_catalogo'})

    return df_val_tags




def adicion_columna_freqs_clase(df_completa_info):
    '''

    Generación de frecuencias de cada SKU.

    :parameters
    :param df_completa_info: base de datos con información del xml.

    :return: Adiciona columna de frecuencias de SKU al dataframe.

    '''

    # frecuencia por clase, par omitir aquelas que sean de menor clase a 2 (prueba).
    freqs_clase = df_completa_info.groupby(['clase_sku'])['filename'].count().reset_index()
    freqs_clase.columns = ['clase_sku', 'conteo_clase']

    # adiciòn de columna con la freqs de cada clase - join
    df_completa_info = df_completa_info.join(freqs_clase.set_index('clase_sku'), on = 'clase_sku')

    return df_completa_info




def adicion_variable_grupo(df, grupo_coleccion):
    '''

    Extracción de grupo (ag, dirigido, otro) y ruta de origen del archivo xml/imagen.

    :parameters
    :param df: base de datos con informaciòn extraída del xml.
    :param grupo_coleccion: grupo de colección dado por el usuario.

    :return: Adición grupo colección a la base de datos.

    '''

    # agregar grupo
    df['grupo'] = 'sin_grupo'

    for index, row in df.iterrows():

        if '/ag/' in row['path_xml_origen']:

            df.loc[index, 'grupo'] = 'ag'

        elif '/dirigido/' in row['path_xml_origen']:

            df.loc[index, 'grupo'] = 'dirigido'

        elif '/'+ grupo_coleccion +'/' in row['path_xml_origen']:

            df.loc[index, 'grupo'] = grupo_coleccion

    return df




def crear_dataset_informacion_completa(list_acumuladora_paths, bucket_pais, path_descarga_mvirt_catalogo,
                                       fecha, dict_catalogos, path_images_consolidadas, grupo_coleccion, path_proceso):
    '''

    Crear base de datos con todas las imágenes/xml con campos de interes y criterios para filtrar
    imágenes que se usaran en el entrenamiento. ademàs de exportar a gcp y carpeta resultados
    train_finalizado_fecha.

    :parameters
    :param list_acumuladora_paths: lista con rutas únicas de las imágenes y xml
    :param bucket_pais: nombre del país que se va a entrenar.
    :param path_descarga_mvirt_catalogo: ruta donde està ubicado el catalogo.csv
    :param fecha: fecha de ejecución (año_mes_dia_hora_min)
    :param dict_catalogos: diccionario catalogos
    :param path_images_consolidadas: ruta carepta imágenes procesadas.
    :param grupo_coleccion: grupo dado por el usuario, default: ag, dirigido.
    :param path_proceso: ruta de carpeta local donde se realiza el procesamiento del programa

    :return: base de datos con información de los xml/imágenes para entrenar y evaluar el modelo.

    '''

    # listas
    list_directory = os.listdir(path_images_consolidadas)

    path_xml_mvirtual = [os.path.join(path_images_consolidadas, i) for i in list_directory if i.endswith('.xml')]

    ## concatenar dataframe de todas las xml/imágenes.
    df_completa_info = pd.DataFrame()

    for file in path_xml_mvirtual:

        # ruta xml e imagen en carpeta consolidada y lista unicos paths
        df = check_bbox(file, list_acumuladora_paths)

        df_val_tags = validar_etiqueta(bucket_pais, path_descarga_mvirt_catalogo, df, dict_catalogos)

        df_completa_info = pd.concat([df_val_tags, df_completa_info], sort=True)

    # Necesario resetear index, evita error la creaciòn de la columna 'grupo_coleccion'
    df_completa_info.reset_index(drop=True, inplace= True)

    # adición frecuencia por clase
    df_completa_info = adicion_columna_freqs_clase(df_completa_info)

    # adicion grupo img (gs ò dirigido u otro)
    df_completa_info = adicion_variable_grupo(df_completa_info, grupo_coleccion)

    # columna  con las bbox que son para training y las que se omiten
    df_completa_info['imagen_train_model'] = ((df_completa_info['nombre_sku_catalogo'] != 'error_etiqueta') &
                                              (df_completa_info['bbox_mayor_img'] != 1) &
                                              (df_completa_info['conteo_clase'] > 1))

    df_completa_info['imagen_train_model'] = df_completa_info['imagen_train_model'].apply(lambda x: 'model' if x == True else 'omitida')


    path_df_completa = os.path.join(path_proceso, 'base_datos_informacion_completa_{}.csv'.format(bucket_pais + '_' + fecha))

    # Exportar DF con información completa a gcp

    columns = ['path_xml_origen', 'path_images_origen', 'filename', 'grupo', 'ancho_img', 'alto_img', 'clase_sku', 'xmin', 'ymin', 'xmax', 'ymax',
               'bbox_menor_tolerancia', 'bbox_mayor_img', 'nombre_sku_catalogo', 'conteo_clase', 'imagen_train_model']

    df_completa_info[columns].to_csv(path_df_completa, sep=',')

    # base de datos cargada a gcp
    subir_archivo(bucket_pais, bucket_pais + '_' + fecha, path_df_completa)

    print(' *** Base de datos exportada a la ruta:\n', 'gs://{}/{}/base_datos_informacion_completa.csv ***'.format(bucket_pais, bucket_pais + '_' + fecha))

    # filtrar las bbox buenas del df completa para el modelo en la ruta 'carpeta imagenes_consolidadas'
    df_data_model = df_completa_info[df_completa_info['imagen_train_model'] == 'model'][['path_xml']]

    # Eliminar carpeta Catalogo
    #shutil.rmtree(path_descarga_mvirt_catalogo)

    return df_data_model


## Split TEST Y TRAIN

def crear_split_mover_train_test(bucket_pais, path_training_files, df_data_model, path_images_consolidadas,split_data, path_proceso):
    '''

    Se crean carpetas de train y test donde se almacena las imágenes/xml.

    :parameters
    :param bucket_pais: nombre del país que se va a entrenar.
    :param path_training_files: ruta de gcp para cargar archivos para entrenamiento (ckpts, tfrecords, pipeline).
    :param df_data_model: base de datos con rutas de imágenes y xml.
    :param path_images_consolidadas: ruta de imágenes procesadas y unificadas.
    :param split_data: proporción para partición e train y test.
    :param path_proceso: ruta de carpeta donde se realiza el procesamiento del programa.

    :return: rutas de carpetas train y test con imágenes/xml y número de clases en el conjunto de datos.

    '''

    # generar Label Map dataset completo
    _, output_label_map, num_class, _ = generar_tfrecord_labelmap(path_images_consolidadas,'dataset_completo', path_proceso)

    #subir label mapa GCP
    # se sube a gcp este label map de dataset completo  porque podrìa reescribirse con el de test/train
    subir_archivo(bucket_pais, path_training_files, output_label_map)

    # crear foldes de particiòn
    path_train_destino = os.path.join(path_proceso,'train')
    path_test_destino = os.path.join(path_proceso,'test')

    try:

        os.mkdir(path_train_destino)
        os.mkdir(path_test_destino)

    except FileExistsError:

        pass

    # path_images a copiar a la ruta carpeta train y test
    train_xml, test_xml = train_test_split(df_data_model['path_xml'].unique(), test_size=split_data)

    # copiar a los trains
    for file in train_xml:
        shutil.copy(file, path_train_destino) # imagen
        shutil.copy(file.replace('.xml','.jpg'), path_train_destino) # xml

    # copiar a los test
    for file in test_xml:
        shutil.copy(file, path_test_destino) # imagen
        shutil.copy(file.replace('.xml','.jpg'), path_test_destino)  # xml

    # Eliminar carpeta imágenes consolidada
    shutil.rmtree(path_images_consolidadas)

    return (path_train_destino, path_test_destino, num_class)


# Tf RECORD y label map

def generar_tfrecord_labelmap(img_directory, dataset, path_proceso):
    '''
    Crear label map y tfrecords de train y test.

    :parameters
    :param img_directory: ruta donde están las imágenes y xml (data completa, train o test)
    :param dataset: string con opciones de 'train', 'test' y 'data completa'.
    :param path_proceso: ruta de carpeta donde se realiza el procesamiento del programa.

    :return: rutas donde se almacena tfrecords y labelmap (carpeta train_finalizado_pais_fecha)

    '''


    print(' *** Generando Tfrecord ***')

    try:
        os.mkdir(os.path.join(path_proceso,'tfrecord_label_map'))

    except FileExistsError:

        pass

    try:

        output_label_map = os.path.join(path_proceso, 'tfrecord_label_map', 'label_map.pbtxt')
        output_tfrecord = os.path.join(path_proceso, 'tfrecord_label_map', '{}.record'.format(dataset))

    except FileExistsError:

            pass


    if dataset == 'dataset_completo':

        # Create and write label map if it doesn't exist
        xmls = get_xmls(img_directory)
        unique_classes = get_unique_classes(xmls)
        unique_ids = assign_unique_ids(unique_classes)
        classes_dict = create_classes_dict(unique_classes, unique_ids)
        label_map_string = create_label_map(classes_dict)

        if not Path(output_label_map).is_file():
            write_label_map(label_map_string, output_label_map)

        print("Creado label map {}! ".format(dataset))

    else:

        # Create and write label map if it doesn't exist
        xmls = get_xmls(img_directory)
        unique_classes = get_unique_classes(xmls)
        unique_ids = assign_unique_ids(unique_classes)
        classes_dict = create_classes_dict(unique_classes, unique_ids)
        label_map_string = create_label_map(classes_dict)

        if not Path(output_label_map).is_file():
            write_label_map(label_map_string, output_label_map)

        # Create and write tfrecord
        writer = tf.python_io.TFRecordWriter(output_tfrecord)
        path = Path.cwd() / img_directory
        # Create dataframe
        xml_df = xml_to_csv(img_directory)
        grouped = split(xml_df, 'filename')

        for group in grouped:
            tf_example = create_tf_example(group, path, classes_dict)
            writer.write(tf_example.SerializeToString())

        writer.close()
        print("Creado TFRecords_{} !".format(dataset))

        # Eliminar carpetas train, test.
        #shutil.rmtree(img_directory)

    return (output_tfrecord, output_label_map, len(unique_classes), os.path.join(path_proceso,'tfrecord_label_map'))




def tf_record_pipeline_and_subir_gcp(bucket_pais, path_training_files, path_train_destino, path_test_destino,
                                    path_file_config, num_class, path_proceso):
    '''
    Generar y Subir archivos tfrecords a gcp, modificar pipeline.config para enviar job entrenamiento.

    :parameters
    :param bucket_pais: nombre del país que se va a entrenar.
    :param path_training_files: ruta gcp donde se cargan los archivos ckpts, pipeline, tfrecords.
    :param path_train_destino: ruta donde està ubicado las imágenes/xml para train.
    :param path_test_destino: ruta donde està ubicado las imágenes/xml para test.
    :param path_file_config: ruta donde se sube el archivo pipeline.config sin modificar
    :param num_class: número de clases (sku) existentes en el conjunto de datos.
    :param path_proceso: ruta de carpeta donde se realiza el procesamiento del programa.

    :return: ruta del pipeline.config modificado.

    '''

    # tf records train
    output_tfrecord_train, _, _, _ = generar_tfrecord_labelmap(path_train_destino, 'train', path_proceso)

    # tf record TEST
    output_tfrecord_test, _, _, path_tfrecords = generar_tfrecord_labelmap(path_test_destino, 'eval', path_proceso)

    # Subir archivos a gcp training files
    subir_archivo(bucket_pais, path_training_files, output_tfrecord_train)
    subir_archivo(bucket_pais, path_training_files, output_tfrecord_test)

    # modificar file config se sube directamente a gcp/training files
    path_config_modificado = cambios_file_config(bucket_pais, path_training_files, path_file_config, num_class)

    # Eliminar carpeta  trrecod_label_map
    #shutil.rmtree(path_tfrecords)


    return path_config_modificado




def job_training_tfboard(bucket_pais, path_output, path_paquetes, path_config_modificado, num_train_steps, fecha):
    '''

    Envío job de entrenamiento a gcp.

    :parameters
    :param bucket_pais: nombre del país que se va a entrenar.
    :param path_output: ruta de gcp donde se almacena los pesos del modelo entrenado en cada steps y los eventos
                        para tfboard
    :param path_paquetes: ruta de gcp donde se almacenan los tres archivos comprimidos (tar.gz) de la API
                          del obejct detection.
    :param path_config_modificado: ruta del pipeline.config modeificado.
    :param num_train_steps: usuario ingresa nùmero de pasos para el entrenamiento.
    :param fecha: fecha de ejecucción.

    :return: Final proceso.

    '''

    # generacion de JOB
    print('**** Generando envío del job de entrenamiento a GCP ***')

    comando_job_training_gcp = 'gcloud beta ai-platform jobs submit training object_detection_`date +%m_%d_%Y_%H_%M_%S` \
    --job-dir gs://{}/{} \
    --packages {},{},{} \
    --module-name object_detection.model_main \
    --region us-central1 \
    --python-version 3.5 \
    --runtime-version 1.13 \
    --scale-tier CUSTOM \
    --master-machine-type standard_v100 \
    -- \
    --model_dir gs://{}/{} \
    --pipeline_config_path {} \
    --num_train_steps {} \
    --sample_1_of_n_eval_examples=1 \
    --alsologtostderr'.format(bucket_pais, path_output, path_paquetes[1], path_paquetes[3], path_paquetes[2],
                              bucket_pais, path_output, path_config_modificado, num_train_steps)

    # envìo de comando
    os.system(comando_job_training_gcp)

    # Tensorboard
    print('**** TENSORBOARD  ****  \n '
           
          '** Copiar este comando en su consola local para visualizar entrenamiento con Tensorboard *** \n '
           
          'tensorboard --logdir gs://{}/{} \n'.format(bucket_pais, path_output))

    #os.system('tensorboard --logdir gs://{}/{}'.format(bucket_pais, path_output))

    # mover carpeta training_pais_fecha al backup de maquina virtual.
    os.system('sudo mv train_finalizado_{}_{} /discos/data_procesada/'.format(bucket_pais, fecha))

    print('*** Información usada para el entrenamiento està en la ruta /discos/data_procesada/ ***')

    #os.system('sudo mv train_finalizado_{}_{} /home/tinamica/Documentos/'.format(bucket_pais, fecha))

    return print('*** Proceso Finalizado ***')
