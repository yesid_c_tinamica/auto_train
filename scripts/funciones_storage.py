'''Funciones de GCP con librerìa Client'''

# Librería
from google.cloud import storage


def crear_bucket(bucket_name):
    ''' crea bueckt en gcs'''
    client = storage.Client()
    bucket = client.bucket(bucket_name)
    bucket.create()


def download_file_images(source_bucket_name, file_name):
    ''' descarga archivo de un bucket específíco en gcs'''

    client = storage.Client()
    bucket = client.get_bucket(source_bucket_name)
    blob = bucket.blob(file_name)
    image_string = blob.download_as_string()
    return image_string


def download_blob(bucket_name, source_blob_name, destination_file_name):
    ''' descarga un blob del bucket en gcs'''

    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(destination_file_name)


def crear_carpeta_bucket(bucket_name, folder_path):
    ''' crea carpeta en un bucket en gcs'''

    # comprobar que el folder_path termina en '/'
    if folder_path[-1] != '/':
        folder_path = folder_path + '/'

    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(folder_path)
    blob.upload_from_string('', content_type='application/x-www-form-urlencoded;charset=UTF-8')


def subir_archivo(bucket_name, ruta_gcp, ruta_local):
    """  subir archivo a gcs desde local """

    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    file_name = ruta_local.split('/')[-1]
    ruta_gcp = ruta_gcp + '/' + file_name
    blob = bucket.blob(ruta_gcp)
    blob.upload_from_filename(ruta_local)


def copy_blob(bucket_name, blob_name, destination_bucket_name, destination_blob_name
):
    """ Copia un blob desde un bucket a otro con un nuevo nombre """

    storage_client = storage.Client()

    source_bucket = storage_client.bucket(bucket_name)
    source_blob = source_bucket.blob(blob_name)
    destination_bucket = storage_client.bucket(destination_bucket_name)

    blob_copy = source_bucket.copy_blob(
        source_blob, destination_bucket, destination_blob_name )

def list_buckets():
    """Lists todos los  buckets """

    storage_client = storage.Client()
    buckets = storage_client.list_buckets()
    lista = []
    for bucket in buckets:
        #print(bucket.name)
        lista.append(bucket.name)

    return lista


def exist_bucket(bucket_name):
    """
    Devuelve un boolean dependiendo si existe o no el bucket
    """
    storage_client = storage.Client()
    return storage_client.bucket(bucket_name).exists()


def obtain_all_files_in_bucket(bucket_name, file):
    ''' obtiene todos los archivos de un bucket '''

    client = storage.Client()
    all_files = []
    blob_list = []
    for blob in client.list_blobs(bucket_name, prefix=file):
        all_files.append(blob.name)
        blob_list.append(blob)
    return all_files, blob_list

