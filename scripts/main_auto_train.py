''' Script Función Principal '''

from funciones_procesamiento import *
from crear_buckets_files_inicial import *
import argparse



def automatizacion_training(path_images_gcp, bucket_files_iniciales, bucket_pais, num_train_steps, grupo_coleccion, split_data):
    '''
    Realiza pre-procesamiento de la data y envìa Job de entrenamiento.

    Parámetros:

        path_images_gcp: Ruta de GCP o local.
        bucket_files_iniciales: Nombre bucket donde estàn ubicados los archivos iniciales,
                                default es bucket a_files_inicial
        bucket_pais: Nombre del país a entrenar.
        num_train_steps: Nùmero de pasos en el entrenamiento. default: 100000
        grupo_coleccion: Grupo adicional dado por el usuario, default: nothing
        split_data: Proporción correspondiente al cojunto de datos 'test', default = 0.10

    Resultado:

        Envìo del Job de entrenamiento a gcp y su comando de visualizaciòn en tensorboard.

    '''

    # Crear bucket, carpetas y cargar algunos archivos iniciales de training
    (fecha, path_paquetes, path_training_files, path_file_config, path_output,
     path_descarga_mvirt_catalogo, dict_catalogos, path_proceso) = crear_bucket_blob_gcp(bucket_pais, bucket_files_iniciales)

    # Comprobar de si el path en entrada es local o de gcp para descargar de la nube.
    path_mvirtual_descarga_img = mvirtual_descarga_dataset_gcp(path_images_gcp, path_proceso)

    # verifica replicas de imagenes/xml
    verificar_files_replicados(path_mvirtual_descarga_img, bucket_pais, fecha, path_proceso)

    # Unificar imágenes descargadas/local y convertir a jpg
    list_acumuladora_paths, path_images_consolidadas = consolidar_and_mover_images_xml_convert_jpg(path_mvirtual_descarga_img, path_proceso)

    # Data frame con información completa de data training
    df_data_model = crear_dataset_informacion_completa(list_acumuladora_paths, bucket_pais, path_descarga_mvirt_catalogo,
                                                       fecha, dict_catalogos, path_images_consolidadas, grupo_coleccion, path_proceso)

    # Split data en test y train
    path_train_destino, path_test_destino, num_class = crear_split_mover_train_test(bucket_pais, path_training_files, df_data_model,
                                                                                    path_images_consolidadas, split_data, path_proceso)
    # Generar tfrecord y subirlos a GCP, modificar pipeline.
    path_config_modificado = tf_record_pipeline_and_subir_gcp(bucket_pais, path_training_files, path_train_destino,
                                                              path_test_destino, path_file_config, num_class, path_proceso)

    # Envío Job Training gcp
    job_training_tfboard(bucket_pais, path_output, path_paquetes, path_config_modificado, num_train_steps, fecha)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description = 'Ruta carpeta a descargar de GCP')

    parser.add_argument('-d', '--directory', type =  str , required= True,
                        help = 'Ruta de la carpeta a descargar de GCP, Ejemplo: gs://pais/../nombre_carpeta_descargar')

    parser.add_argument('--name_bucket_pais', type=str, required=True, help='Nombre del país a entrenar')

    parser.add_argument('--name_bucket_archivos_iniciales', type =  str , required= False,
                        default= 'a_c_paquetes_todos_los_paises', help = 'Ruta de la carpeta donde estàn los archivos inicales')

    parser.add_argument('--num_train_steps', type=int, required=False, default=100000,
                        help='Número de pasos para entrenar el modelo, default = 100000')

    parser.add_argument('--grupo_coleccion', type=str, required=False, default='nothing', help='grupo colecciòn adicional')

    parser.add_argument('--split_data', type=float, required=False, default=0.10,
                        help='Proporción para la partición del dataset (train/test), default = 0.10')


    args = parser.parse_args()

    automatizacion_training(args.directory, args.name_bucket_archivos_iniciales, args.name_bucket_pais,
                            args.num_train_steps, args.grupo_coleccion, args.split_data)








