''' Modificar pipeline.config '''

## librerías

import tensorflow as tf
from google.protobuf import text_format  # Scripts complemento para leer el .config
from object_detection.protos import pipeline_pb2  # mètodos de la API 


def cambios_file_config(pais, path_training_files, path_file_config, num_clases):
  '''
  Asigna las rutas correspondientes al pais que se va a entrenar en el archivo pipeline.config.

  :param pais: nombre del país que se va a entrenar.
  :param path_training_files: ruta gcp donde se cargan los archivos ckpts, pipeline, tfrecords.
  :param path_file_config: ruta donde està almacenado el pipeline.config sin modificaciones.
  :param num_clases: número de SKU identificados en el conjunto de datos.

  :return: ruta del pipeline.config modificado

  '''

  pipeline = pipeline_pb2.TrainEvalPipelineConfig()

  # read  archivo .config

  with tf.gfile.GFile('gs://{}pipeline.config'.format(path_file_config), "r") as f:
      proto_str = f.read()
      text_format.Merge(proto_str, pipeline)

  #  Asiginaciòn nuevas rutas
  pipeline.model.faster_rcnn.num_classes = int(num_clases)
  pipeline.train_input_reader.tf_record_input_reader.input_path[:] = ['gs://{}/{}/train.record'.format(pais, path_training_files)]
  pipeline.train_input_reader.label_map_path = 'gs://{}/{}/label_map.pbtxt'.format(pais, path_training_files)

  pipeline.eval_input_reader[0].tf_record_input_reader.input_path[:] = ['gs://{}/{}/eval.record'.format(pais, path_training_files)]
  pipeline.eval_input_reader[0].label_map_path = 'gs://{}/{}/label_map.pbtxt'.format(pais, path_training_files)

  pipeline.train_config.fine_tune_checkpoint = 'gs://{}/{}/model.ckpt'.format(pais, path_training_files)

  config_text = text_format.MessageToString(pipeline)
  with tf.gfile.Open('gs://{}/{}/pipeline.config'.format(pais, path_training_files), "wb") as f:
    f.write(config_text)

  return 'gs://{}/{}/pipeline.config'.format(pais, path_training_files)

